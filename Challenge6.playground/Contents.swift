import UIKit

var cashOnHand: Double = 2000
var runningCash: Double = 10.0
var percentGain: Double = 10.0
var yearsToInvest: Int = 5
var yearsElapsed: Int = 0

runningCash = cashOnHand
percentGain /= 100

repeat {
    runningCash += (runningCash * percentGain)
    print(runningCash)
    yearsElapsed += 1
} while yearsElapsed < yearsToInvest
