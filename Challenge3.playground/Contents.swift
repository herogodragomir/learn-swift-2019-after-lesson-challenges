import UIKit

var lastName: String = "Cu"
var firstName: String = "Edy"
let gender: String = "Male"
var age: Int = 35
var cashOnHand: Double = 100000.00
var hasChildren: Bool = false

if hasChildren {
    print("Being a parent is hard, my money goes to my children instead of games!")
} else if age > 18 {
    print("Adulting is hard I can't buy the game because I need to pay bills.")
} else {
    print("I'm young and I can do what I want so gimme that game!")
}
